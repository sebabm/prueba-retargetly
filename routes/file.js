const express = require("express");
const router = express.Router();
const fs = require('fs');



router.get('/', function (req, res) {
  let nombre_completo = req.query.filename;
  filePath = './upload/' + nombre_completo;

  res.download(filePath, function (err) {
    if (err) {
      return res.status(404).json({
        success: false,
        error: 'Archivo no encontrado'
      });
    } 
  })
});

router.post('/', function (req, res) {
  let archivo;
  let uploadPath;
  let extension;

  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).json({
      success: false,
      error: 'No se encontro nigun archivo.'
    });
  }
  archivoUpload = req.files.file;
  archivo = new Date().getTime().toString()
  extension = archivoUpload.name.split('.').pop();

  nombre_completo = archivo + '.' + extension;

  uploadPath = './upload/' + nombre_completo;

  // Use the mv() method to place the file somewhere on your server
  archivoUpload.mv(uploadPath, function (err) {
    if (err)
      return res.status(500).json({
        success: false,
        error: err
      });

    res.status(200).json({
      success: true,
      nombre_completo: nombre_completo
    });

  });
});



module.exports = router;
