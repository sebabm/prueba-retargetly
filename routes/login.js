const express = require("express");
const jwt = require("jsonwebtoken");
const { now } = require("mongoose");
const router = express.Router();
const User = require("../models/User");

router.post(
  "/login",
  async (req, res) => {
    const error = req.username && req.password;

    if (error) {
      return res.status(400).json({
        success: false,
        error: 'Se necesitan ingresar todos los datos.'
      });
    }

    const { username, password } = req.body;
    try {
      let user = await User.findOne({
        username
      });
      if (!user)
        return res.status(400).json({
          success: false,
          error: 'Usuario inexistente.'
        });

      const isMatch = password === user.password;
      if (!isMatch)
        return res.status(400).json({
          success: false,
          error: 'Contraseña incorrecta.'
        });

      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        process.env.SEMILLA,
        {
          expiresIn: 3600
        },
        async (err, token) => {
          if (err) { throw err; }
          else {
            User.updateOne({ username }, { token, updatedAt: now() }, function (err, res) {
              if (err) throw console.log(err);
              console.log('Registros modificados: ' + JSON.stringify(res));
            });

            let loginUser = await User.findOne({
              token
            });

            res.status(200).json({
              success: true,
              user: loginUser
            });
          }
        }
      );
    } catch (e) {
      console.error(e);
      res.status(500).json({
        success: false,
        message: "Server Error",
        error: e.message
      });
    }
  }
);


module.exports = router;
