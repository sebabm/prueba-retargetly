const express = require("express");
const router = express.Router();
const PlatformClient = require("../models/PlatformClient");


router.get('/', function (req, res) {
  let sort = req.query.sort ? req.query.sort : 'ASC';
  let sortField = req.query.sortField ? req.query.sortField : '_id';
  let limit = req.query.limit ? parseInt(req.query.limit) : 1000;
  sort = sort == 'ASC' ? 1 : -1;

  console.log([{ [sortField]: sort }, limit])

  PlatformClient.find()
    .sort({ [sortField]: sort })
    .limit(limit)
    .then(platforms => {
      // console.log(platforms)
      res.status(200).json({
        success: true,
        platforms
      });
    });
});


module.exports = router;
