const mongoose = require("mongoose");

// Replace this with your MONGOURI.
const MONGOURI = "mongodb://localhost/retargetly";

const InitiateMongoServer = async () => {
  try {
    await mongoose.connect(MONGOURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log("Conexion a la BD OK!!");
  } catch (e) {
    console.log(e);
    throw e;
  }
};



module.exports = InitiateMongoServer;