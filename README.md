# Pasos para correr el proyecto
- Clonar proyecto 
- Importar BD de mongo en formato json desde carpeta ```mongo_db_backup/retargetly/json``` o exportada desde mongodump en la carpeta ```mongo_db_backup/retargetly/mongodump```
- Ejecutar comando ```npm install```
- Crear archivo  ```.env``` con la semilla de JWT con la siguiente variable ```SEMILLA=r3t4rg3tly**@8@```
- Ejecutar proyecto con el comando ```npm run nodemon``` (Desarrollo con hotreload) ```npm star``` (Sin hotreload)
- (Opcional) Importar la coleccion de [POSTMAN para pruebas](https://www.getpostman.com/collections/9e0200cb8ac30aeb38d8) 



## Mejoras a la consigna
- Se realizo una funcion que importa el CSV al iniciar el proyecto
- El CSV se obtiene de ```/config/platform_client.csv```
- Se creo el modelo ```User```
- Se configuro un archivo ```.env``` para almacenar la semilla de JWT
- Se adjunta un [archivo de configuración de POSTMAN](https://www.getpostman.com/collections/9e0200cb8ac30aeb38d8) para probar los endpoints

## Dependencias
- [csv-parser](https://www.npmjs.com/package/csv-parser) para almacenar el CSV
- [express-fileupload](https://www.npmjs.com/package/express-fileupload) para subir los archivos en la ruta /file
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) JWT en login y endpoints
- [mongoose](https://mongoosejs.com/) para realizar el modelo de la BD



