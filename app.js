var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require("dotenv").config();
const InitiateMongoServer = require("./config/db");
const PlatformClient = require("./models/PlatformClient");
const csv = require('csv-parser');
const fs = require('fs');
const fileUpload = require('express-fileupload');



function actualizarCSV() {
  return fs.createReadStream(__dirname + '/config/platform_client.csv')
    .pipe(csv())
    .on('data', (data) => {
      const platform = new PlatformClient(data);

      platform.save(function (err, product) {
        if (err) {
          throw err;
          console.log(err);
        } else {
          // console.log(product);
        }
      })

    })
    .on('end', () => {
      console.log('Importado correctamente');
    });
}

const deleteAllData = async () => {
  try {
    await PlatformClient.deleteMany();
    console.log('Se borro la coleccion platform_client');
  } catch (err) {
    console.log(err);
  }
};

// Iniciar servidor Mongo
InitiateMongoServer().then(() => {
  //Limpia la coleccion que existia
  deleteAllData();
  //Guarda el nuevo CSV
  actualizarCSV();
}
);


const login = require('./routes/login');
const data = require('./routes/data');
const file = require('./routes/file');

//Middleware para chequear Token
const middlewareCheckLogin = require('./middleware/checkLogin');


var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());

app.use('/', login);
app.use('/data', middlewareCheckLogin, data);
app.use('/file', middlewareCheckLogin, file);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err,
    success: false
  });
});

module.exports = app;
