const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const UserSchema = mongoose.Schema({
  _id: {
    type: Schema.Types.ObjectId,
  },
  username: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  name: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: false,
    default: null,
    unique: true 
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: {
    type: Date,
    default: Date.now()
  }
});


// export model user with UserSchema
module.exports = mongoose.model("user", UserSchema);
