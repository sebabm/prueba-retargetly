var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlatformClientSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    segment1: {
        type: Boolean,
        required: true,
    },
    segment2: {
        type: Boolean,
        required: true,
    },
    segment3: {
        type: Boolean,
        required: true,
    },
    segment4: {
        type: Boolean,
        required: true,
    },
    platformId: {
        type: Number,
        required: true,
    },
    clientId: {
        type: Number,
        required: true,
    },
});


module.exports = mongoose.model("platform_client", PlatformClientSchema);

